import os
import cv2
import numpy
from functools import wraps
from datetime import datetime
from skimage.measure import compare_psnr as psnr
import multiprocessing
import ssim as pyssim
from PIL import Image
import matplotlib.pyplot as plt

"""
Apt packages required:-
 . python2.7
 . python-opencv

Pip packages required:-
 . scikit-image
 . pyssim

"""

DEBUG = True


class Consumer(multiprocessing.Process):
    def __init__(self, task_queue, result_queue):
        multiprocessing.Process.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue

    def run(self):
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                # Poison pill means shutdown
                self.task_queue.task_done()
                break
            answer = next_task()
            self.task_queue.task_done()
            self.result_queue.put(answer)
        return


class Measure(object):
    def __init__(self, frame_number, frame_ref, frame_test, measure_psnr=True, measure_ssim=True, upscale=True):
        self.frame_ref = frame_ref
        self.frame_test = frame_test
        self.frame_number = frame_number
        self.measure_psnr = measure_psnr
        self.measure_ssim = measure_ssim
        self.upscale = upscale

    def __call__(self):
        ret = list()
        ret.append(self.frame_number)
        # Do we need to rescale the test frame to match the reference frame?
        if self.frame_ref.shape != self.frame_test.shape:
            if self.upscale:
                # Need to match shape of reference
                old_shape = self.frame_test.shape
                self.frame_test = cv2.resize(self.frame_test, (self.frame_ref.shape[1], self.frame_ref.shape[0]))
                if self.frame_number == 1:
                    print "NOTE: Up-scaling test clip to match reference clip (%sx%s to %sx%s)" % \
                          (str(old_shape[1]),
                           str(old_shape[0]),
                           str(self.frame_test.shape[1]),
                           str(self.frame_test.shape[0]))

        if self.measure_psnr:
            ret.append(psnr(self.frame_ref, self.frame_test))
        if self.measure_ssim:
            ret.append(pyssim.compute_ssim(Image.fromarray(self.frame_ref), Image.fromarray(self.frame_test)))

        return ret


def timed(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        start = datetime.now()
        result = f(*args, **kwargs)
        elapsed = datetime.now() - start
        if DEBUG:
            print "[%s timed at %s seconds]" % (f.__name__, (str(elapsed.seconds) + "." + str(elapsed.microseconds)))
        return result
    return wrapper


class ObjectiveVideoQualityMeasure(object):

    @timed
    def compare_files(self, file_ref, file_test, frame_count=100, trigger_threshold=250,
                      measure_psnr=True, measure_ssim=True, trigger_on_white_frame=True,
                      upscale=True, plot=True):
        ref = cv2.VideoCapture()
        if not ref.open(file_ref):
            raise ObjectiveVideoQualityMeasureError("Unable to find file '%s'" % file_ref)

        test = cv2.VideoCapture()
        if not test.open(file_test):
            raise ObjectiveVideoQualityMeasureError("Unable to find file '%s'" % file_test)

        if trigger_on_white_frame:
            # Advance to trigger frame in ref clip
            print "Finding trigger frame in file_ref..."

            while 1:
                ret, frame = ref.read()
                if not ret:
                    raise ObjectiveVideoQualityMeasureError("Trigger frame not found")
                if numpy.average(frame) > trigger_threshold:
                    break

            # Advance to trigger frame in test clip
            print "Finding trigger frame in file_test..."

            while 1:
                ret, frame = test.read()
                if not ret:
                    raise ObjectiveVideoQualityMeasureError("Trigger frame not found")
                if numpy.average(frame) > trigger_threshold:
                    break

        print "Measuring %s frames...." % frame_count

        tasks = multiprocessing.JoinableQueue()
        results = multiprocessing.Queue()

        # Start consumers, 2 for each processor
        num_consumers = multiprocessing.cpu_count() * 2
        consumers = [Consumer(tasks, results)
                     for _ in xrange(num_consumers)]

        for w in consumers:
            w.start()

        # Enqueue measurement jobs
        current_frame = 1
        while 1:
            ret, frame_ref = ref.read()
            if not ret:
                raise ObjectiveVideoQualityMeasureError("Reference file did not have enough frames")
            ret, frame_test = test.read()
            if not ret:
                raise ObjectiveVideoQualityMeasureError("Test file did not have enough frames")

            tasks.put(Measure(current_frame, frame_ref, frame_test, measure_psnr, measure_ssim, upscale))
            if current_frame == frame_count:
                break
            else:
                current_frame += 1

        # Add poison pill to each consumer
        for i in xrange(num_consumers):
            tasks.put(None)

        tasks.join()

        # Convert queue to sorted list
        results_list = []
        while frame_count:
            g = results.get()
            results_list.append(g)
            frame_count -= 1

        results_list.sort()

        vectors = self.redim_results(results_list)
        if plot:
            self.plot_vectors(vectors, zoom=False, title="'%s' vs '%s'" % (os.path.split(file_ref)[-1],
                                                                           os.path.split(file_test)[-1]))

        return results_list, vectors

    @staticmethod
    def redim_results(results_array):
        results_array.sort()  # ensure they are in frame order
        vectors = []

        for x in range(len(results_array[0])):
            if x == 0:
                pass
            else:
                vector = []
                for list_item in results_array:
                    vector.append(list_item[x])
                vectors.append(vector)

        return vectors

    @staticmethod
    def plot_vectors(*vectors, **kwargs):

        title = "FR Video Quality"
        zoom = False

        for arg in kwargs:
            if arg == "title":
                title = kwargs[arg]
            if arg == "zoom":
                zoom = kwargs['zoom']

        psnr_data = list()
        ssim_data = list()
        psnr_index = 0
        ssim_index = 1

        max_vector_size = 0

        for vector in vectors:
            if len(vector[psnr_index]) > max_vector_size:
                max_vector_size = len(vector[psnr_index])

        print "max_vector_size = %s" % str(max_vector_size)

        for x in range(max_vector_size):
            column_data = list()
            for vector in vectors:
                if x > len(vector[psnr_index]):
                    column_data.append(None)
                else:
                    column_data.append(vector[psnr_index][x])
            psnr_data.append(column_data)

        for x in range(max_vector_size):
            column_data = list()
            for vector in vectors:
                if x > len(vector[ssim_index]):
                    column_data.append(None)
                else:
                    column_data.append(vector[ssim_index][x])
            ssim_data.append(column_data)

        fig, ax1 = plt.subplots()

        t = numpy.arange(1, max_vector_size + 1, 1)

        # Plot left y scale for PSNR
        ax1.plot(t, psnr_data, 'b-')
        ax1.set_xlabel("frame")

        ax1.set_ylabel('PSNR (dB)', color='b')
        if not zoom:
            ax1.set_ylim([0.0, 50.0])
        ax1.tick_params('y', colors='b')

        # Plot right y scale for SSIM
        ax2 = ax1.twinx()
        ax2.plot(t, ssim_data, 'r-.')
        ax2.set_ylabel('SSIM (1.0 = 100%)', color='r')
        ax2.tick_params('y', colors='r')
        if not zoom:
            ax2.set_ylim([0.6, 1.0])

        fig.tight_layout()
        plt.gcf().canvas.set_window_title(title)
        plt.show()


class ObjectiveVideoQualityMeasureError(RuntimeError):
    pass
